﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TesteIdentityCore.Data;
using TesteIdentityCore.Models;


namespace TesteIdentityCore.Controllers
{
    public class ControllerBase : Microsoft.AspNetCore.Mvc.Controller
    {
        public readonly ILogger<HomeController> _logger;
        public readonly ApplicationDbContext _context;

        public ControllerBase(ILogger<HomeController> logger, ApplicationDbContext context)
        {
            _logger = logger;
            _context = context;
        }
    }
}
