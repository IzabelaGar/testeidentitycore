﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TesteIdentityCore.Data;
using TesteIdentityCore.Models;

namespace TesteIdentityCore.Controllers
{
    [Authorize]
    public class HomeController : ControllerBase
    {
        
        public HomeController(ILogger<HomeController> logger, ApplicationDbContext context) : base(logger,context)
        {
        }

        public IActionResult Index()
        {
            var t = _context.Users.ToList();
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
